VENV = .venv310
SRC_DIR = src
CACHE_DIR := .make_cache

LINT := pylint
STYLE := pycodestyle
TYPE := mypy
TEST := pytest
BUILD := python -m build
DEPLOY := twine upload --repository testpypi
DEPLOY_PROD := twine upload

SRC_ABS = $(abspath $(SRC_DIR))
MOD_FILES := $(shell find $(SRC_DIR) -type d -name '__pycache__' -prune \
			 -o -type f -name '*.py' ! -name '*_test.py'  -print)
TEST_FILES := $(shell find $(SRC_DIR) -type d -name '__pycache__' -prune \
			 -o -type f \
			 \( -name 'test_*.py' -o -name '*_test.py' \) -print)
CACHE_FILES := $(foreach f,$(MOD_FILES) $(TEST_FILES),$(CACHE_DIR)/$(f))
PYCACHE_DIRS := $(shell find . -maxdepth 1 -type d -name '__pycache__' \
			&& find $(SRC_DIR) -type d -name '__pycache__')
PYPATH := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
LINT := PYTHONPATH=$(PYPATH) $(LINT)
STYLE := PYTHONPATH=$(PYPATH) $(STYLE)
TYPE := PYTHONPATH=$(PYPATH) $(TYPE)
TEST := PYTHONPATH=$(PYPATH) $(TEST)
.SHELLFLAGS := -ec

.ONESHELL:
.PHONY: all help lint style type test build deploy prod clean debug \
	$(MOD_FILES) $(TEST_FILES)

# Run all the holistic check/test targets
all: lint style type test
	@$(foreach f,$(CACHE_FILES),
		mkdir -p $(dir $(f))
		touch $(f))

help:
	@printf "usage: make [<target>]\n\n"
	printf "targets:\n\n"
	printf "  all (default)	Check and test the src and test dirs\n\n"
	printf "  lint          Lint the src and test dirs\n\n"
	printf "  style         Style check the src and test dirs\n\n"
	printf "  type          Type check the src and test dirs\n\n"
	printf "  test          Test the src and test dirs\n\n"
	printf "  <module_file>	Check the module and run any available tests\n"
	printf "		if the module hasn't been checked/tested\n"
	printf "		since it was last modified\n\n"
	printf "  <test_file>	Check the test and run it if the test hasn't\n"
	printf "		been checked/tested since it was last\n"
	printf "                modified\n\n"
	printf "  modules       Check/test any modules that haven't been\n"
	printf "                since they were last modified\n\n"
	printf "  tests         Check/test any tests that haven't been since\n"
	printf "                they were last modified\n\n"
	printf "  build	        Build the package for deployment\n\n"
	printf "  deploy	Deploy the package to the test repo\n\n"
	printf "  prod  	Deploy the package to the prod repo\n\n"
	printf "  clean	        Delete cache and build files\n\n"
	printf "  help		Print this message\n\n"
	printf "  debug		Print some variables\n"

# These 4 targets run holistically on directories, instead of an individual file
lint:
	@printf '%s %s\n' '$(LINT)' '$(SRC_DIR)'
	$(LINT) $(SRC_DIR)

style:
	@printf '%s %s\n' '$(STYLE)' '$(SRC_DIR)'
	$(STYLE) $(SRC_DIR)

type:
	@printf '%s %s\n' '$(TYPE)' '$(SRC_DIR)'
	$(TYPE) $(SRC_DIR)

test:
	@printf '%s %s\n' '$(TEST)' '$(SRC_DIR)'
	$(TEST) $(SRC_DIR)

# Runs checks/tests on individual files that have changed
$(MOD_FILES):
#	We actually have a target: prerequisite relationship of
#		$(CACHE_DIR)/module.py: $(SRC_DIR)/module.py
#	But it would be annoying to call make $(CACHE_DIR)/module.py, so we
#	basically invert the relationship and check that the module target is
#	newer than the cache file before running checks/tests, then create the
#	cache file to record when the module was last checked/tested
	@if [[ '$@' -nt '$(CACHE_DIR)/$@' ]]; then
		printf '%s %s\n' '$(LINT)' '$@'
		$(LINT) $@
		printf '%s %s\n' '$(STYLE)' '$@'
		$(STYLE) $@
		printf '%s %s\n' '$(TYPE)' '$@'
		$(TYPE) $@
#		there is a corresponding test file in the same directory
		if [[ -f '$(basename $@)_test.py' ]]; then
			$(TEST) $(basename $@)_test.py
		fi
#		touch cache file to record module was just checked/tested
		mkdir -p '$(CACHE_DIR)/$(@D)'
		touch '$(CACHE_DIR)/$@'
	else
		printf "make: '%s' is up to date.\n" '$@'
	fi

# Run individual tests that have changed
$(TEST_FILES):
#	We do the same thing we did for modules, and check that the test target
#	is newer than the cache file before running the checks/test
	@if [[ '$@' -nt '$(CACHE_DIR)/$@' ]]; then
		printf '%s %s\n' '$(LINT)' '$@'
		$(LINT) $@
		printf '%s %s\n' '$(STYLE)' '$@'
		$(STYLE) $@
		printf '%s %s\n' '$(TYPE)' '$@'
		$(TYPE) $@
		printf '%s %s\n' '$(TEST)' '$@'
		$(TEST) $@
#		touch cache file to record test was just checked/ran
		mkdir -p '$(CACHE_DIR)/$(@D)'
		touch '$(CACHE_DIR)/$@'
	else
		printf "make: '%s' is up to date.\n" '$@'
	fi

# Runs checks/tests on all individual files that have changed
modules: $(MOD_FILES)

# Run all individual tests that have changed
tests: $(TEST_FILES)

build:
	@$(RM) -rf $(SRC_DIR)/*.egg-info
	$(BUILD)

deploy:
	@name="$$(sed -nE -e 's/^name ?= ?(.*)/\1/p' setup.cfg)"
	name_dash="$${name//_/-}"
	name_us="$${name//-/_}"
	version="$$(sed -nE -e 's/^version ?= ?(.*)/\1/p' setup.cfg)"
	$(DEPLOY) dist/{"$${name_dash}","$${name_us}"}-"$${version}"*

prod:
	@name="$$(sed -nE -e 's/^name ?= ?(.*)/\1/p' setup.cfg)"
	name_dash="$${name//_/-}"
	name_us="$${name//-/_}"
	version="$$(sed -nE -e 's/^version ?= ?(.*)/\1/p' setup.cfg)"
	$(DEPLOY_PROD) dist/{"$${name_dash}","$${name_us}"}-"$${version}"*

# Delete cache files which will enable all checks/tests to run again, and
# build files
clean:
	@$(RM) -rf $(CACHE_DIR)/* $(PYCACHE_DIRS)
	$(RM) -rf $(SRC_DIR)/*.egg-info

debug:
	@printf 'SRC_DIR:\t%s\n' "${SRC_DIR}"
	printf 'SRC_ABS:\t%s\n' "${SRC_ABS}"
	printf 'MOD_FILES:\t%s\n' "${MOD_FILES}"
	printf 'TEST_FILES:\t%s\n' "${TEST_FILES}"
	printf 'CACHE_FILES:\t%s\n' "${CACHE_FILES}"
	printf 'PYCACHE_DIRS:\t%s\n' "${PYCACHE_DIRS}"
	printf 'PYPATH:\t\t%s\n' "${PYPATH}"
	printf 'LINT:\t\t%s\n' "${LINT}"
	printf 'STYLE:\t\t%s\n' "${STYLE}"
	printf 'TYPE:\t\t%s\n' "${TYPE}"
	printf 'TEST:\t\t%s\n' "${TEST}"
	printf 'BUILD:\t\t%s\n' "${BUILD}"
	printf 'DEPLOY:\t\t%s\n' "${DEPLOY}"
	printf 'DEPLOY_PROD:\t%s\n' "${DEPLOY_PROD}"
	printf '.SHELLFLAGS:\t%s\n' "${.SHELLFLAGS}"

