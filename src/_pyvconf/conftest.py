"""pytest shared fixtures."""

import logging
from typing import Generator

import pytest


@pytest.fixture(scope="package", autouse=True)
def logger() -> Generator[logging.Logger, None, None]:
    """A debugging logger."""
    res = logging.getLogger("pytest")
    res.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler(filename="pytest.log", mode="w",
                                       encoding="utf-8")
    file_handler.setFormatter(logging.Formatter(
        "%(asctime)s %(filename)s %(message)s"))
    res.addHandler(file_handler)
    res.debug("test started")
    yield res
    res.debug("test finished\n**********")

