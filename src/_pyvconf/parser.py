"""Config parser interface and classes."""

from abc import ABC, abstractmethod
from collections.abc import Mapping, MutableMapping, Sequence
import configparser as cp
from dataclasses import dataclass, field
from typing import cast, Literal

from .types import FieldName, FieldValue, Config, ConfigFlat

MutableConfigFlat = MutableMapping[FieldName, FieldValue]


class IParser(ABC):  # pylint: disable=too-few-public-methods
    """Interface to parse a config source."""

    @abstractmethod
    def parse(self) -> Config | ConfigFlat:
        """Parse a config source."""


@dataclass
class CPKwargs:  # pylint: disable=too-many-instance-attributes
    """Data class to hold the kwarg options for parsing an INI file."""
    defaults: Mapping[FieldName, FieldValue] | None = None
    # Since allow_no_value defaults to False, the only value it can be set
    # later to is True. However, we have to use the cast so the type checker
    # won't complain when setting the default value to False.
    allow_no_value: Literal[True] = cast(Literal[True], False)
    delimiters: Sequence[str] = ("=", ":")
    comment_prefixes: Sequence[str] = ("#", ";")
    inline_comment_prefixes: Sequence[str] | None = None
    strict: bool = True
    empty_lines_in_values: bool = True
    default_section: str = cp.DEFAULTSECT
    interpolation: cp.Interpolation | None = field(
        default_factory=cp.BasicInterpolation)


@dataclass
class ParserIni(IParser):  # pylint: disable=too-few-public-methods
    """Parses an INI file into a dict."""
    ini_path: str
    # If we don't provide this type hint, pylint gets confused
    cp_kwargs: CPKwargs = field(default_factory=CPKwargs)

    def parse(self) -> Config:
        """Parse an INI file."""
        parser = cp.ConfigParser(**self.cp_kwargs.__dict__)
        parser.read(self.ini_path)

        # Build the dict manually so it contains plain dicts, and not funky
        # stuff like SectionProxies
        config = {}
        if parser.default_section:
            config[parser.default_section] = dict(parser.defaults())
        for section in parser.sections():
            config[section] = dict(parser[section])

        return config


@dataclass
class ParserIniFlat(IParser):  # pylint: disable=too-few-public-methods
    """Parses an INI file into a flattened dict."""
    ini_path: str
    # If we don't provide this type hint, pylint gets confused
    cp_kwargs: CPKwargs = field(default_factory=CPKwargs)

    def parse(self) -> ConfigFlat:
        """Parse an INI file and flatten the sections."""
        # Prevent the ConfigParser from processing a default section, so
        # regular sections will not pickup default values. Consider a situation
        # where you have a default section, and two regular sections. If the
        # first regular section has a value, but the second one does not,
        # the second one will pick up the default value. Now when you flatten
        # the results, the default value in the second section will be used,
        # instead of the value from the first section. By not adding default
        # values into regular sections, we can get the correct flat dict
        # by adding each section's dict to an empty dict, starting with the
        # default section.
        default_section = self.cp_kwargs.default_section
        self.cp_kwargs.default_section = ""
        parser = cp.ConfigParser(**self.cp_kwargs.__dict__)
        parser.read(self.ini_path)

        # Make sure the default section comes first
        sections = parser.sections()
        if sections[0] != default_section and default_section in sections:
            sections.remove(default_section)
            sections.insert(0, default_section)
        config: MutableConfigFlat = {}
        for section in sections:
            config.update(parser[section])

        return config

