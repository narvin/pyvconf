"""Unit tests for the Validator class."""

import pytest

from pyvconf import (Checker, Converter, FieldValue, FieldValueT as VT,
                     FieldValueRawT as RVT, Validator)


class TestValidator:
    """Encapsulates the unit tests for the Validator class."""

    @staticmethod
    def test_instantiate() -> None:
        """Test instantiating a Validator."""
        validator: Validator[FieldValue, FieldValue] = Validator()

        assert isinstance(validator, Validator)

    @staticmethod
    @pytest.mark.parametrize("raw_value, value", [
        ("hello", "hello"),
        (1, 1),
        (1.5, 1.5),
        (False, False),
    ])
    def test_empty_validator(raw_value: RVT, value: VT) -> None:
        """Test an empty validator."""
        validator: Validator[RVT, VT] = Validator()

        assert validator.validate(raw_value) == value

    @staticmethod
    @pytest.mark.parametrize("convert, raw_value, value", [
        (str, 123, "123"),
        (int, "1", 1),
        (float, "1.5", 1.5),
        (bool, "true", True),
        (bool, "false", True),
        (Validator.to_bool, "true", True),
        (Validator.to_bool, "whatever", True),
        (Validator.to_bool, 1, True),
        (Validator.to_bool, 1.0, True),
        (Validator.to_bool, "false", False),
        (Validator.to_bool, "FALSE", False),
        (Validator.to_bool, "no", False),
        (Validator.to_bool, "No", False),
        (Validator.to_bool, "", False),
        (Validator.to_bool, 0, False),
        (Validator.to_bool, 0.0, False),
    ])
    def test_converter(convert: Converter[RVT, VT],
                       raw_value: RVT, value: VT) -> None:
        """Test validator converters."""
        validator: Validator[RVT, VT] = Validator(convert)

        assert validator.validate(raw_value) == value

    @staticmethod
    @pytest.mark.parametrize("check, in_value, value", [
        (Validator.create_num_in_range(0, 10), 1, 1),
        (Validator.create_num_in_range(0, 10), 1.0, 1.0),
        (Validator.create_num_in_range(-1, 1), 0, 0),
        (Validator.create_num_in_range(-1.0, 1.0), 0, 0),
        (Validator.create_num_in_range(0, 10), 0, 0),
        (Validator.create_num_in_range(0, 10), 10, 10),
    ])
    def test_checker(check: Checker[VT], in_value: RVT, value: VT) -> None:
        """Test validator checkers."""
        validator: Validator[RVT, VT] = Validator(checks=check)

        assert validator.validate(in_value) == value

    @staticmethod
    @pytest.mark.parametrize("check, in_value, rng", [
        (Validator.create_num_in_range(0, 10), -1, "[0, 10]"),
        (Validator.create_num_in_range(0, 10), 11.0, "[0, 10]"),
        (Validator.create_num_in_range(-1, 1), 2, "[-1, 1]"),
        (Validator.create_num_in_range(-1.0, 1.0), -1.1, "[-1.0, 1.0]"),
        (Validator.create_num_in_range(0, 10, True, True), 0, "(0, 10)"),
        (Validator.create_num_in_range(0, 10, True, True), 10, "(0, 10)"),
    ])
    def test_checker_raises(check: Checker[VT], in_value: RVT,
                            rng: str) -> None:
        """Test validator checkers raise."""
        validator: Validator[RVT, VT] = Validator(checks=check)

        with pytest.raises(ValueError) as excinfo:
            validator.validate(in_value)
        assert f"must be in range {rng}: {in_value}" in str(excinfo.value)

