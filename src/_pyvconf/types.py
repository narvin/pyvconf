"""Types for the package."""

from collections.abc import Mapping
from typing import Callable, TypeVar

SectionName = str
FieldName = str
FieldValue = str | None
Config = Mapping[SectionName, Mapping[FieldName, FieldValue]]
ConfigFlat = Mapping[FieldName, FieldValue]

FieldValueT = TypeVar("FieldValueT")
FieldValueRawT = TypeVar("FieldValueRawT")
Converter = Callable[[FieldValueRawT], FieldValueT]
Checker = Callable[[FieldValueT], FieldValueT]

