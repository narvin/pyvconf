"""Unit tests for the parser module."""

# pylint: disable=too-few-public-methods

import configparser
import json
import logging
import os
import tempfile
from typing import Generator, Tuple, Type

from _pytest.fixtures import SubRequest
import pytest

from pyvconf import (Config, ConfigFlat, CPKwargs, IParser, ParserIni,
                     ParserIniFlat)


class TestAbstractParser:
    """Encapsulates the unit tests for the abstract parser class."""

    @staticmethod
    def test_instantiate_fails() -> None:
        """Test instantiating the abstract parser class fails."""
        with pytest.raises(TypeError) as excinfo:
            # pylint: disable=abstract-class-instantiated
            IParser()  # type: ignore

        assert "Can't instantiate abstract class" in str(excinfo.value)


class TestKwargsClass:
    """Encapsulates the unit tests for the kwargs data class."""

    @staticmethod
    def test_has_attrs() -> None:
        """Test that the class has the default attrs."""
        cp_kwargs = CPKwargs()

        assert {"defaults", "allow_no_value", "delimiters", "comment_prefixes",
                "inline_comment_prefixes", "strict", "empty_lines_in_values",
                "default_section", "interpolation"} == cp_kwargs.__dict__.keys()


class TestIniParsers:
    """Encapsulates the unit tests for the ini parser classes."""

    IniFileRes = Tuple[str, Config, ConfigFlat, CPKwargs]

    @staticmethod
    @pytest.fixture(scope="class", params=[
        # Tuples consisting of:
        # - the dict used to create the ini file
        # - the expected dict from a section parser
        # - the expected dict from a flat parser
        # - optional configparser kwargs

        # Single regular section will pick up default values it is missing
        (
            {
                "DEFAULT": {"str": "1", "int": "1", "float": "1", "bool": "1"},
                "s1": {"str": "2", "float": "1.5"},
            },
            {
                "DEFAULT": {"str": "1", "int": "1", "float": "1", "bool": "1"},
                "s1": {"str": "2", "float": "1.5", "int": "1", "bool": "1"},
            },
            {"str": "2", "float": "1.5", "int": "1", "bool": "1"},
        ),

        # Single regular will not pick up any default values because it is
        # not missing any values for which there are defaults
        (
            {
                "DEFAULT": {"str": "2", "float": "1.5"},
                "s1": {"str": "1", "int": "1", "float": "1", "bool": "1"},
            },
            {
                "DEFAULT": {"str": "2", "float": "1.5"},
                "s1": {"str": "1", "int": "1", "float": "1", "bool": "1"},
            },
            {"str": "1", "int": "1", "float": "1", "bool": "1"},
        ),

        # Two regular sections, where the second will pick up a default
        # value. The flattened result will contain the value from the first
        # section that is missing from the second.
        (
            {
                "DEFAULT": {"str": "2", "float": "1.5"},
                "s1": {"str": "1", "int": "1", "float": "1", "bool": "1"},
                "s2": {"int": "1", "float": "1", "bool": "1"},
            },
            {
                "DEFAULT": {"str": "2", "float": "1.5"},
                "s1": {"str": "1", "int": "1", "float": "1", "bool": "1"},
                "s2": {"str": "2", "int": "1", "float": "1", "bool": "1"},
            },
            # str value is from s1
            {"str": "1", "int": "1", "float": "1", "bool": "1"},
        ),

        # Like the case above, except the sections are not in alphabetical order
        (
            {
                "DEFAULT": {"str": "2", "float": "1.5"},
                "s2": {"str": "1", "int": "1", "float": "1", "bool": "1"},
                "s1": {"int": "1", "float": "1", "bool": "1"},
            },
            {
                "DEFAULT": {"str": "2", "float": "1.5"},
                "s2": {"str": "1", "int": "1", "float": "1", "bool": "1"},
                "s1": {"str": "2", "int": "1", "float": "1", "bool": "1"},
            },
            # str value is from s2
            {"str": "1", "int": "1", "float": "1", "bool": "1"},
        ),

        # The default section is last, and contains a value that neither of
        # the the two regular sections do
        (
            {
                "s1": {"str": "1", "int": "1", "float": "1"},
                "s2": {"int": "2", "float": "2.0"},
                "DEFAULT": {"str": "2", "float": "1.5", "foo": "1"},
            },
            {
                "DEFAULT": {"str": "2", "float": "1.5", "foo": "1"},
                "s1": {"str": "1", "int": "1", "float": "1", "foo": "1"},
                "s2": {"str": "2", "int": "2", "float": "2.0", "foo": "1"},
            },
            # str value is from s1
            {"str": "1", "int": "2", "float": "2.0", "foo": "1"},
        ),

        # The default section does not have have the default name, and there
        # is a third empty section
        (
            {
                "common": {"def": "0", "s": "0"},
                "s1": {"s": "1"},
                "s2": {"s": "2"},
                "s3": {},
            },
            {
                "common": {"def": "0", "s": "0"},
                "s1": {"def": "0", "s": "1"},
                "s2": {"def": "0", "s": "2"},
                "s3": {"def": "0", "s": "0"},
            },
            {"def": "0", "s": "2"},
            CPKwargs(default_section="common"),
        ),
    ])
    def ini_file(request: SubRequest,
                 logger: logging.Logger) -> Generator[IniFileRes, None, None]:
        """Create an ini file and return its path."""
        ini_spec, exp_sections, exp_fields, *cp_kwargs = request.param
        cp_kwargs = cp_kwargs[0] if cp_kwargs else CPKwargs()
        logger.debug("ini_spec: %s", json.dumps(ini_spec, indent=4))
        with tempfile.NamedTemporaryFile(mode="w+t", delete=False) as ini_fp:
            # No default section causes the ini_spec to be written to the file
            # in order, otherwise the default section would be written first
            config = configparser.ConfigParser(default_section="")
            config.read_dict(ini_spec)
            config.write(ini_fp)
            ini_fp.seek(0)
            logger.debug("%s\n%s", ini_fp.name, ini_fp.read().strip())
        yield (ini_fp.name, exp_sections, exp_fields, cp_kwargs)
        os.remove(ini_fp.name)

    @staticmethod
    @pytest.mark.parametrize("parser_class", [ParserIni, ParserIniFlat])
    def test_instantiate(parser_class: Type[ParserIniFlat | ParserIni]) -> None:
        """Test instantiating an IniParser."""
        parser = parser_class("path/to/ini_file")

        assert isinstance(parser, parser_class)
        assert isinstance(parser, IParser)

    @staticmethod
    def test_parse(ini_file: IniFileRes, logger: logging.Logger) -> None:
        """Test parsing an INI file."""
        ini_path, expected, _, cp_kwargs = ini_file
        parser = ParserIni(ini_path, cp_kwargs)
        config = parser.parse()

        logger.debug("ParserIni.parse: %s", json.dumps(config, indent=4))
        logger.debug("expected: %s", json.dumps(expected, indent=4))
        assert config == expected

    @staticmethod
    def test_flat_parse(ini_file: IniFileRes, logger: logging.Logger) -> None:
        """Test parsing an INI file."""
        ini_path, _, expected, cp_kwargs = ini_file
        parser = ParserIniFlat(ini_path, cp_kwargs)
        config = parser.parse()

        logger.debug("ParserIniFlat: %s", json.dumps(config, indent=4))
        logger.debug("expected: %s", json.dumps(expected, indent=4))
        assert config == expected

