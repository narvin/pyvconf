"""Convert and validation classes and functions."""

from functools import reduce
from typing import cast, Generic, MutableSequence, Optional

from .types import Checker, Converter, FieldValueT, FieldValueRawT


class Validator(Generic[FieldValueRawT, FieldValueT]):
    """Converts and validates a value."""

    def __init__(
        self,
        convert: Optional[Converter[FieldValueRawT, FieldValueT]] = None,
        checks: Optional[
            MutableSequence[Checker[FieldValueT]]
            | Checker[FieldValueT]
        ] = None,
    ) -> None:
        self.convert = convert
        if not checks:
            self.checks: MutableSequence[Checker[FieldValueT]] = []
        elif not isinstance(checks, MutableSequence):
            self.checks = [checks]
        else:
            self.checks = checks

    def append_check(self, check: Checker[FieldValueT]) -> None:
        """Append a check on the value."""
        self.checks.append(check)

    def validate(self, raw_value: FieldValueRawT) -> FieldValueT:
        """Convert the value, and apply all checks/tranforms."""
        value = self.convert(raw_value) if self.convert \
            else cast(FieldValueT, raw_value)
        return reduce(lambda acc, check: check(acc), self.checks, value)

    @staticmethod
    def to_bool(raw_value: FieldValueRawT) -> bool:
        """Converts a value to a bool, and handles additonal falsy values."""
        if isinstance(raw_value, str):
            if raw_value.lower() in ('false', 'no'):
                return False
        return bool(raw_value)

    @staticmethod
    def create_num_in_range(min_value: float, max_value: float,
                            lower_open: bool = False,
                            upper_open: bool = False) -> Checker[float]:
        """Factory to create a num_in_range function."""
        from operator import lt, le  # pylint: disable=import-outside-toplevel

        if lower_open:
            lower_cmp = lt
            lower_mark = '('
        else:
            lower_cmp = le
            lower_mark = '['
        if upper_open:
            upper_cmp = lt
            upper_mark = ')'
        else:
            upper_cmp = le
            upper_mark = ']'

        def num_in_range(value: float) -> float:
            if lower_cmp(min_value, value) and upper_cmp(value, max_value):
                return value

            rng = f"{lower_mark}{min_value}, {max_value}{upper_mark}"
            raise ValueError("invalid value for num_in_range: "
                             + f"must be in range {rng}: {value}")

        return num_in_range

